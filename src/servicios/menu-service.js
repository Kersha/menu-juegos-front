const obtenerItems = () => {
    return [
        {
          id: 1,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
            "Salsas extra"
          ],
        },
        {
          id: 2,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 3,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 4,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
            "Martes 2 x1"
          ],
        },
        {
          id: 5,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 6,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 7,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 8,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 9,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 10,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
        {
          id: 11,
          title: "Hamburguesa dos carnes",
          description: [
            "Disfruta de una hamburguesa de 250g de carne de res con salsa barbeque, lechuga, tomate y pepinillos",
          ],
        },
      ]
}

export const MenuService = {
    obtenerItems,
};
