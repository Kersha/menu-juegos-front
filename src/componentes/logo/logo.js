import React, { useState } from "react";
import "./logo.css";

const Logo = ({ name, logoUrl, phrase }) => {
  const [disappear, setDisappear] = useState(false);

  return (
    <React.Fragment>
      <img
        alt="logo tienda"
        className={`store-logo background-logo ${
          disappear ? "reduce-torightbot-animation" : ""
        }`}
        src={logoUrl}
      ></img>
      <div
        className={`welcome ${
          disappear ? "disappear-animation" : "appearfromtop-animation"
        }`}
      >
        <div className="store-logo-cont">
          <img className="store-logo" src={logoUrl} alt="logo tienda"></img>
        </div>
        <div className="store-name">{name}</div>
        <div className="store-phrase">{phrase}</div>
        <button className="welcome-button" onClick={() => setDisappear(true)}>
          Vér el menú
        </button>
      </div>
    </React.Fragment>
  );
};

export default Logo;
