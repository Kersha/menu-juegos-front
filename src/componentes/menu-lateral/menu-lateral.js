import React from "react";
import ItemMenuLateral from "./item-menu-lateral/item-menu-lateral";
import "./menu-lateral.css";
import { BiSearch } from "react-icons/bi";

const MenuLateral = () => {
  const options = [
    { title: "Salado", icon: "GiAxeInLog", id: 1 },
    { title: "Dulce", icon: "GiAxeInLog", id: 2 },
    { title: "Extras", icon: "GiAxeInLog", id: 3 },
    { title: "Todo", icon: "GiAxeInLog", id: 4 },
    { title: "Algo", icon: "GiAxeInLog", id: 5 },
  ];

  return (
    <div className="menu-lateral-container">
      <div className="menu-title">
        <h2>Hamburguesas a lo bestia</h2>
        <h1>Menú</h1>
      </div>
      <div className="option option-search">
        <BiSearch className="option-icon"></BiSearch>
        <label htmlFor="search" className="option-title">
          Buscar
        </label>
      </div>
      <div className="option option-search">
        <input type="text" id="search" placeholder="nombre platillo" className="search-input"></input>
      </div>
      {options.map((option) => (
        <div className="option" key={option.id}>
          <ItemMenuLateral {...option}></ItemMenuLateral>
        </div>
      ))}
    </div>
  );
};

export default MenuLateral;
