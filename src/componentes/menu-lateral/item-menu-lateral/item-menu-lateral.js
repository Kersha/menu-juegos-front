import React from "react";
import './item-menu-lateral.css';
import { BiRestaurant } from "react-icons/bi";

const ItemMenuLateral = (props) => {
  return (
    <div className="item-menu">
      <BiRestaurant className="option-icon"></BiRestaurant>
      <span className="option-title">{props.title}</span>
    </div>
  );
};

export default ItemMenuLateral;