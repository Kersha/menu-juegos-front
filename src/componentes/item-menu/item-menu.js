import React from "react";
import "./item-menu.css";
import hamburguesa from "../../assets/hamburguesa.jpg";

const ItemMenu = (props) => {
  return (
    <div className="itemContainer appear-cool">
      <div className="imgContainer">
        <img src={hamburguesa} className="itemImage" alt="comida"></img>
      </div>
      <h4 className="itemTitle">{props.title}</h4>
      <ul className="itemDescription">
        {props.description.map((descrip) => (
          <li key={descrip}>{descrip}</li>
        ))}
      </ul>
    </div>
  );
};
export default ItemMenu;
