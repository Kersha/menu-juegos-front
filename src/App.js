import React, { useState, useEffect } from "react";
import "./App.css";
import ItemMenu from "./componentes/item-menu/item-menu";
import { MenuService } from "./servicios/menu-service";
import MenuLateral from "./componentes/menu-lateral/menu-lateral";
import Logo from "./componentes/logo/logo";

function App() {
  const [search, setSearch] = useState("");
  const [items, setItems] = useState([]);

  useEffect(() => setItems(MenuService.obtenerItems()), [search]);

  const store= {
    name: 'Hamburguesas a lo bestia',
    logoUrl: 'https://images.vexels.com/media/users/3/158440/isolated/preview/849336c3ea34f256725db5da5ffb6c4f-burger-logo-comida-logotipo-silueta-by-vexels.png',
    phrase: 'Para hambres voraces'
  }

  return (
    <React.Fragment>
    <Logo {...store}></Logo>
      <MenuLateral></MenuLateral>
      <div className="row">
        <div className="items items-first-col">
          {items.slice(0, Math.ceil(items.length / 2)).map((item) => (
            <div className="item" key={item.id}>
              <ItemMenu {...item} key={item.id}></ItemMenu>
            </div>
          ))}
        </div>
        <div className="items">
          {items.slice(Math.ceil(items.length / 2)).map((item) => (
            <div className="item" key={item.id}>
              <ItemMenu {...item} key={item.id}></ItemMenu>
            </div>
          ))}
        </div>
      </div>
    </React.Fragment>
  );
}

export default App;
